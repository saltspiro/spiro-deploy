FROM python:alpine

WORKDIR /tmp/spiro

COPY . /tmp/spiro

RUN apk add --no-cache git && \
    pip install .
